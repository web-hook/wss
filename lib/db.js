import { MongoClient } from 'mongodb';
import { logger } from './log.js'

// Keep track of DB connection
let db;

// Connect to the DB
export async function connectToDB() {
  try {
      const uri = process.env.MONGODB_URL || 'mongodb://mongo:27017';
      const mongoClient = new MongoClient(uri);
      logger.info('connecting to MongoDB...');
      await mongoClient.connect();
      logger.info('connected to MongoDB');
      db = mongoClient.db('webhooks');
  } catch (error) {
      logger.error('connection to MongoDB failed!', error);
      process.exit();
  }
}

// Make sure token references an existing webhook
export async function getWebhookFromToken(token) {
  if (!db) {
    await connectToDB()()
  }

  const collection = db.collection('webhooks');
  const webhooks = await collection.find({"token": token}).toArray();

  const [firstWebhook = null] = webhooks || []

  return firstWebhook;
}

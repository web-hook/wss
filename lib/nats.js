import { connect, StringCodec, credsAuthenticator } from 'nats';
import * as fs from 'fs';
import { clients } from './wss.js';
import { logger } from './log.js'

export async function handleMessages() {
  let msg, json_msg;

  // NATS connection string
  const uri = process.env.NATS_URL || 'nats://nats:4222';

  // NATS connection options
  let options = { servers:  uri };

  // Add credentials if provided
  if (fs.existsSync('/nats/creds')) {
    // read credentials file
    let creds = fs.readFileSync("/nats/creds", "utf-8");
    options["authenticator"] = credsAuthenticator(new TextEncoder().encode(creds));
  } 

  // Connect to NATS
  const nc = await connect(options);

  // Using codec for decoding incoming messages
  const sc = StringCodec();

  //
  // Subscribe to all messages coming on the data.* subject
  // Each message should have the following format:
  // {
  //   "content": ...,    // json object
  //   "created_at: ..., // timestamp in UTC format
  //   "webhook": ...    // name of the webhook the data are sent to
  // }
  //
  const sub = nc.subscribe('data.*');

  for await (const m of sub) {
    // Get webhook name from subject
    const { subject } = m;
    logger.info(`new message received on NATS subject:${subject}`);

    const [,webhook_name] = subject.split('.') || [];

    try {
      // Extract data payload
      const data = sc.decode(m.data);
      logger.info(data);

      // Make sure socket still exists before sending message to clients
      const ws = clients.get(webhook_name);
      if(!ws){
        logger.error(`no websocket client for webhook ${webhook_name}`);
      } else {
        ws.send(data);
        logger.debug('message sent to websocket');
      }

    } catch (error) {
      logger.error('error handling incoming message', error);
    }
  }

  // Subscription has been closed somehow
  logger.info('subscription closed');
};

import { createServer } from 'http';
import { WebSocketServer } from 'ws';
import { getWebhookFromToken } from './db.js'
import { parse } from 'url';
import { logger } from './log.js'

// Keep track of connected clients
export const clients = new Map();

// Make sure token has the expected format ([a-fA-F0-9]{6,30})
const regex = new RegExp('^[a-zA-Z0-9]{6,30}$');

export async function runWebsockertServer() {
  // Create http + websocket servers
  // Request will first reach http server to be authenticated then protocol will be upgraded to websocket
  const server = createServer();

  function heartbeat() {
    this.isAlive = true;
  }

  const wss = new WebSocketServer({ noServer: true });

  const interval = setInterval(function ping() {
    logger.debug('about to ping connected websockets...');
    wss.clients.forEach(function each(ws) {
      logger.debug('ping...');
      if (ws.isAlive === false) return ws.terminate();
  
      ws.isAlive = false;
      ws.ping();
    });
  }, 30000);

  wss.on('connection', function connection(ws, request, webhook) {
    logger.info(`new connection [${webhook.name}]`);

    ws.isAlive = true;
    ws.on('pong', heartbeat);

    // Send webhook name to websocket client
    ws.send(`webhook:${webhook.name}`);

    // Keep track of the socket for the current webhook
    clients.set(webhook.name, ws);

    // Handle message received from websocket clients
    ws.on('message', (data) => {
      logger.info('message received');
      logger.info(data);

      // ping/pong
      if(data == 'ping'){
        ws.send('pong');
      }

      // Closing socket if "unsubscribe" message received
      if(data == 'unsubscribe'){
        ws.close();
      }
    });

    // Handle client disconnection
    ws.on('close', () => {
      logger.error('closing socket');
      clients.delete(webhook.name);
    });

    // Handle communication error
    ws.on('error', (err) => {
      logger.error(err);
      clients.delete(webhook.name);
    });
  });

  wss.on('close', function close() {
    clearInterval(interval);
  });

  // Authenticate the request before upgrading the connection
  server.on('upgrade', async function upgrade(request, socket, head) {
    // Parse url to extract token
    const { query } = parse(request.url, true);

    // Make sure token is provided
    if(! query.token){
      logger.error('token is not provided');
      socket.destroy();
      return;
    }

    if(! regex.test(query.token)){
      logger.error('token is invalid');
      socket.destroy();
      return;
    }

    // Check if there is a webhook associated to provided token
    const webhook = await getWebhookFromToken(query.token);
    if(webhook === null){
      logger.error('no webhook associated to the token provided');
      socket.destroy();
      return;
    }

    // Upgrade connextion to websocket protocol
    wss.handleUpgrade(request, socket, head, function done(ws) {
      wss.emit('connection', ws, request, webhook);
    });
  });

  server.listen(8080);
}
